/*
 This file is part of JustGarble.

    JustGarble is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JustGarble is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JustGarble.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "../include/aes.h"
#include "../include/common.h"
#include "../include/util.h"
#include "../include/justGarble.h"
#include <stdio.h>
#include <ctype.h>
#include <stdint.h>
#include <wmmintrin.h>
#include <math.h>

static __m128i cur_seed;

// Count from 0 to n-1 into array a[]
int countToN(int *a, int n) {
	int i;
	for (i = 0; i < n; i++)
		a[i] = i;
	return 0;
}

int dbgBlock(block a) {
	int *A = (int *) &a;
	int i;
	int out = 0;
	for (i = 0; i < 4; i++)
		out = out + (A[i] + 13432) * 23517;
	return out;
}

int compare(const void * a, const void * b) {
	return (*(int*) a - *(int*) b);
}

int median(int *values, int n) {
	int i;
	qsort(values, n, sizeof(int), compare);
	if (n % 2 == 1)
		return values[(n + 1) / 2];
	else
		return (values[n / 2] + values[n / 2 + 1]) / 2;
}

double doubleMean(double *values, int n) {
	int i;
	double total = 0;
	for (i = 0; i < n; i++)
		total += values[i];
	return total / n;
}

// This is only for testing and benchmark purposes. Use a more 
// secure seeding mechanism for actual use.
int already_initialized = 0;
void seedRandom() {
	if (!already_initialized) {
		already_initialized = 1;
		__current_rand_index = zero_block();
		srand(time(NULL));
		block cur_seed = _mm_set_epi32(rand(), rand(), rand(), rand());
		AES_set_encrypt_key((unsigned char *) &cur_seed, 128, &__rand_aes_key);
	}
}

block randomBlock() {
	block out;
	const __m128i *sched = ((__m128i *) (__rand_aes_key.rd_key));
	randAESBlock(&out, sched);
	return out;
}

void print_block(block b) {
	uint8_t *b8 = (uint8_t *) &b;
	printf("%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x", 
	       b8[0], b8[1], b8[2], b8[3], b8[4], b8[5], b8[6], b8[7],
	       b8[8], b8[9], b8[10], b8[11], b8[12], b8[13], b8[14], b8[15]);
}

void print_bin(int *str, unsigned int n) {
	int i;
	for (i = 0; i < n; i++) {
		printf("%d", str[n - 1 - i]);
	}
}

void u642bin(uint64_t v, int *str, unsigned int n) {
	int i;
	for (i = 0; i < n; i++) {
		str[i] = (v & (1 << i)) >> i;
	}
	return;
}

uint64_t bin2u64(int *str, unsigned int n) {
	int i;
	uint64_t v = 0;
	for (i = 0; i < n; i++) {
		 v += str[i] * ((uint64_t) pow(2, i));
	}
	return v;
}
