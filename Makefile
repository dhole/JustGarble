# ***********************************************
#                    JustGarble
# ***********************************************

SRCDIR   = src
OBJDIR   = obj
BINDIR   = bin
TESTDIR   = test
OBJECTFULL = obj/*.o

SOURCES  := $(wildcard $(SRCDIR)/*.c)
INCLUDES := $(wildcard $(SRCDIR)/*.h)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

IDIR =../include
CC=gcc
# DEBUG
#CFLAGS= -std=gnu89 -O0 -g -lm -lrt -lpthread -maes -msse4 -lmsgpackc -march=native -I$(IDIR)
CFLAGS= -std=gnu89 -lm -lrt -lpthread -maes -msse4 -lmsgpackc -march=native -I$(IDIR)


SUM = SumCircuitTest
AES = AESFullTest
LARGE = LargeCircuitTest
FILE = CircuitFileTest
rm = rm --f

all: AES LARGE FILE SUM

SUM: $(OBJECTS) $(TESTDIR)/$(SUM).c
	$(CC) $(OBJECTFULL) $(TESTDIR)/$(SUM).c -o $(BINDIR)/$(SUM).out $(LIBS) $(CFLAGS) 

AES: $(OBJECTS) $(TESTDIR)/$(AES).c
	$(CC) $(OBJECTFULL) $(TESTDIR)/$(AES).c -o $(BINDIR)/$(AES).out $(LIBS) $(CFLAGS) 

LARGE: $(OBJECTS) $(TESTDIR)/$(LARGE).c
	$(CC) $(OBJECTFULL) $(TESTDIR)/$(LARGE).c -o $(BINDIR)/$(LARGE).out $(LIBS) $(CFLAGS) 

FILE: $(OBJECTS) $(TESTDIR)/$(FILE).c
	$(CC) $(OBJECTFULL) $(TESTDIR)/$(FILE).c -o $(BINDIR)/$(FILE).out $(LIBS) $(CFLAGS) 


$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	$(CC) -c $< -o $@ $(LIBS) $(CFLAGS) 

.PHONEY: clean
clean:
	@$(rm) $(OBJECTS)
	@$(rm) $(BINDIR)/$(AES)
	@$(rm) $(BINDIR)/$(LARGE)
	@$(rm) $(BINDIR)/$(FILE)

