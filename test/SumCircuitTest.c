/*
 This file is part of JustGarble.

    JustGarble is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JustGarble is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JustGarble.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "../include/justGarble.h"

#define K 10000

int checkfn(int *inputs, int *outputs, int n) {
	int i;	
	uint64_t in[K], out = 0;
	//printf("Real: sum(");
	for (i = 0; i < K; i++) {
		in[i] = bin2u64(&inputs[i * n/K], n/K);
		//printf("%u, ", in[i]);
	}
	for (i = 0; i < K; i++) {
		out += in[i];
	}
	out = (uint16_t) out;
	//printf(") = %u = ", out);
	u642bin(out, outputs, n/K);
	//print_bin(outputs, n/K);
	//printf("\n");
	return 0;
}

int main(int argc, char **argv) {
	seedRandom();
	GarbledCircuit garbledCircuit;
	GarblingContext garblingContext;

	// ADD22: 2 gates, 2 wires
	// ADD32: 5 gates, 5 wires
	// ADD  : 1 ADD22 + bits ADD32

	int times = 10;
	double t = 0;

	//Set up circuit parameters
	int bits = 16;
	// input
	int n = bits * K;
	// output
	int m = bits;
	// gates
	int q = (1 * 2 + (bits - 1) * 5) * (K - 1);
	printf("Gates = %d\n", q);
	// wires
	int r = n + (1 * 2 + (bits - 1) * 5) * (K - 1);
	printf("Wires = %d\n", r);

	//Set up input and output tokens/labels.
	//block *labels = (block*) malloc(sizeof(block) * 2 * n);
	block *labels = (block*) calloc(2 * n, sizeof(block));
	block *outputbs = (block*) calloc(2 * m, sizeof(block));
	// inputs is the indexes of the input wires
	int *inputs = (int *) malloc(sizeof(int) * n);
	// setup indexes for input wires
	countToN(inputs, n);
	// outputs is the indexes of the output wires
	int outputs[m];

	OutputMap outputMap = outputbs;
	InputLabels inputLabels = labels;

	//Actually build a circuit. Alternatively, this circuit could be read
	//from a file.
	createInputLabels(inputLabels, n);
	createEmptyGarbledCircuit(&garbledCircuit, n, m, q, r, inputLabels);
	startBuilding(&garbledCircuit, &garblingContext);

	// Summation circuit
	int i, j;
	int oldInternalWires[bits];
	int newInternalWires[bits];
	int twoInputs[2 * bits];
	memcpy(oldInternalWires, inputs, sizeof(int) * bits);
	for (i = 0; i < K - 1; i++) {
		//for (j = 0; j < bits; j++) {
		//	newInternalWires[j] = getNextWire(&garblingContext);
		//}
		for (j = 0; j < bits; j++) {
			twoInputs[j]     = oldInternalWires[j];
			twoInputs[bits + j] = inputs[(i + 1) * bits + j];
		}
		ADDCircuit(&garbledCircuit, &garblingContext, bits * 2, twoInputs,
			newInternalWires);

		memcpy(oldInternalWires, newInternalWires, sizeof(int) * bits);
	}
	memcpy(outputs, oldInternalWires, sizeof(int) * bits);

	// K = 2
	//ADDCircuit(&garbledCircuit, &garblingContext, bits * 2, inputs, outputs);

	finishBuilding(&garbledCircuit, &garblingContext, outputMap, outputs);

	//Garble the built circuit.
	garbleCircuit(&garbledCircuit, inputLabels, outputMap);

	//Evaluate the circuit with random values and check the computed
	//values match the outputs of the desired function.
	checkCircuit(&garbledCircuit, inputLabels, outputMap, &(checkfn));

	for (i = 0; i < times; i++) {
		t += timedEvalMs(&garbledCircuit, inputLabels);
	}
	printf("Mean(Eval_time) = %f ms\n", t/times);

	removeGarbledCircuit(&garbledCircuit);
	removeGarblingContext(&garblingContext);

	return 0;
}

